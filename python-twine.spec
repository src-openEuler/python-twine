%global _empty_manifest_terminate_build 0
Name:		python-twine
Version:	5.1.0
Release:	1
Summary:	Collection of utilities for publishing packages on PyPI
License:	Apache-2.0
URL:		https://twine.readthedocs.io/
Source0:	https://files.pythonhosted.org/packages/source/t/twine/twine-%{version}.tar.gz
BuildArch:	noarch

BuildRequires:  python3-hatchling python3-hatch-vcs

Requires:	python3-pkginfo
Requires:	python3-readme-renderer
Requires:	python3-requests
Requires:	python3-requests-toolbelt
Requires:	python3-setuptools
Requires:	python3-keyring
Requires:	python3-rfc3986
Requires:	python3-urllib3
Requires:	python3-importlib-metadata
Requires:	python3-rich

%description
Twine is a utility for publishing Python packages on PyPI.
It provides build system independent uploads of source and binary
distribution artifacts for both new and existing projects.
See our documentation for a description of features, installation
and usage instructions, and links to additional resources.

%package -n python3-twine
Summary:	Collection of utilities for publishing packages on PyPI
Provides:	python-twine
BuildRequires:	python3-devel python3-pip python3-wheel
BuildRequires:	python3-setuptools python3-setuptools_scm
%description -n python3-twine
Twine is a utility for publishing Python packages on PyPI.
It provides build system independent uploads of source and binary
distribution artifacts for both new and existing projects.
See our documentation for a description of features, installation
and usage instructions, and links to additional resources.

%package help
Summary:	Development documents and examples for twine
Provides:	python3-twine-doc
%description help
Twine is a utility for publishing Python packages on PyPI.
It provides build system independent uploads of source and binary
distribution artifacts for both new and existing projects.
See our documentation for a description of features, installation
and usage instructions, and links to additional resources.

%prep
%autosetup -n twine-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}

%files -n python3-twine
%{python3_sitelib}/*
%{_bindir}/twine

%files help
%{_docdir}/*

%changelog
* Thu Aug 29 2024 xu_ping <707078654@qq.com> - 5.1.0-1
- Update package to version 5.1.0
  Remove Python 3.7 from classifiers
  add AttributeError to blake2 check

* Wed Jul 31 2024 dongqi <dongqi1@kylinos.cn> - 5.0.0-1
- Update package to version 5.0.0
- add setuptools to test deps
- Update classifiers for newer Pythons
- drop Python 3.7

* Thu Apr 27 2023 caodongxia <caodongxia@h-partners.com> - 4.0.2-2
- Adapting to the pyproject.toml compilation mode

* Thu Dec 15 2022 fushanqing <fushanqing@kylinos.cn> - 4.0.2-1
- update to version 4.0.2

* Tue Nov 08 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 4.0.1-1
- Upgrade package to version 4.0.1

* Wed May 11 2022 yangping <yangping69@h-partners> - 3.2.0-2
- License compliance rectification

* Wed Nov 18 2020 maminjie <maminje1@huawei.com> - 3.2.0-1
- package init
